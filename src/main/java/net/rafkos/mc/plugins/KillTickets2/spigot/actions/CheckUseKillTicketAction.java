/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.KillTickets2.spigot.actions;

import java.util.HashMap;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.KillTickets2.bungee.KillTicketsBungee;
import net.rafkos.mc.plugins.KillTickets2.bungee.MessagesGenerator;
import net.rafkos.mc.plugins.KillTickets2.bungee.Ticket;
import net.rafkos.mc.plugins.KillTickets2.bungee.Tickets;

@AllArgsConstructor
@Getter
public class CheckUseKillTicketAction implements IAction
{

	private static final long serialVersionUID = 8012190542782739065L;
	private UUID senderUniqueId;
	private String[] args;

	@Override
	public HashMap<String, Object> execute()
	{
		ProxiedPlayer sender = ProxyServer.getInstance().getPlayer(senderUniqueId);
		UUID uuid = UUID.fromString(args[2]);

		String ticketId = args[1];
		Ticket ticket = Tickets.getTicketById(ticketId);
		boolean canUse = false;
		HashMap<String, Object> result = new HashMap<>();
		result.put("ticket", ticket);

		int killsCount = KillTicketsBungee.getKillsDatabase().getKillsCount(senderUniqueId, uuid);
		if(KillTicketsBungee.getKillsDatabase().getKilledPlayersUUIDs(senderUniqueId).contains(uuid))
		{
			if(ticket != null)
			{
				if(Tickets.getTicketsForKills(killsCount, sender).contains(ticket))
				{
					canUse = true;
				}else
				{
					MessagesGenerator.message(sender, "Nie możesz używać podanego biletu.", ChatColor.RED);
				}
			}else
			{
				MessagesGenerator.message(sender, "Podany bilet nie istnieje.", ChatColor.RED);
			}

		}else
		{
			MessagesGenerator.message(sender, "Nie zabiłeś gracza o takiej nazwie.", ChatColor.RED);
		}
		result.put("canUse", canUse);

		return result;
	}

}
