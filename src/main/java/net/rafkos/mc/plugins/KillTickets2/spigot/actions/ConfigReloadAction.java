/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.KillTickets2.spigot.actions;

import java.util.HashMap;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.KillTickets2.bungee.Config;
import net.rafkos.mc.plugins.KillTickets2.bungee.KillTicketsBungee;
import net.rafkos.mc.plugins.KillTickets2.bungee.MessagesGenerator;
import net.rafkos.mc.plugins.KillTickets2.bungee.Tickets;

@AllArgsConstructor
@Getter
public class ConfigReloadAction implements IAction
{
	private static final long serialVersionUID = -6231128238426916410L;
	private UUID senderUniqueId;

	@Override
	public HashMap<String, Object> execute()
	{
		ProxiedPlayer sender = ProxyServer.getInstance().getPlayer(senderUniqueId);
		if(sender.hasPermission("KillTickets.admin"))
		{
			Config.load();
			Tickets.load();
			KillTicketsBungee.getKillsDatabase().load();
			MessagesGenerator.message(sender, "Konfiguracja przeładowana, sprawdź logi. ", ChatColor.GREEN);
		}
		else
		{
			MessagesGenerator.message(sender, "Nie masz uprawnień do wykonania tej komendy.", ChatColor.RED);
		}
		return new HashMap<String, Object>();
	}

}
