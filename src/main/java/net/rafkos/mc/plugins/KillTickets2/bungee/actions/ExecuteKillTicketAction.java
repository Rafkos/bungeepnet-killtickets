/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.KillTickets2.bungee.actions;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.ConsoleCommandSender;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.KillTickets2.bungee.Ticket;
import net.rafkos.mc.plugins.KillTickets2.bungee.TicketCommand;

@AllArgsConstructor
@Getter
@Setter
public class ExecuteKillTicketAction implements IAction
{
	private static final long serialVersionUID = -894147595653349397L;
	private String senderName;
	private UUID targetUniqueId;
	private Ticket ticket;

	@Override
	public HashMap<String, Object> execute()
	{
		HashMap<String, Object> result = new HashMap<>();
		result.put("status", false);

		OfflinePlayer target = Bukkit.getPlayer(targetUniqueId);
		if(!target.isOnline() && (ticket.isOnline() || ticket.isAlive()))
		{
			result.put("msg", "Aby użyć tego biletu gracz musi być online.");
			return result;
		}
		if(target.isOnline() && ticket.isAlive())
		{
			if(target.getPlayer().isDead())
			{
				result.put("msg", "Aby użyć tego biletu gracz musi żyć.");
				return result;
			}
		}
		for(TicketCommand cmd : ticket.getCommands())
		{
			String command = cmd.getCommand();
			String updated = new String(command);
			updated = updated.replace("%TARGET%", target.getName()).replace("%OWNER%", senderName);
			ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
			Bukkit.dispatchCommand(console, updated);
		}

		result.put("status", true);
		return result;
	}

}
