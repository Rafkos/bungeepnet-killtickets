package net.rafkos.mc.plugins.KillTickets2.bungee;

import net.rafkos.mc.plugins.KillTickets2.common.Logger;

public class KillsDatabaseCleaner implements Runnable
{

	private KillsDatabase killsDatabase;

	public KillsDatabaseCleaner(KillsDatabase killsDatabase)
	{
		this.killsDatabase = killsDatabase;
	}

	@Override
	public void run()
	{
		Logger.log("Cleaning kills database");
		killsDatabase.cleanDatabase();
	}

}
