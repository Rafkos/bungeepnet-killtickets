package net.rafkos.mc.plugins.KillTickets2.spigot;

import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetBungeeServer;
import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;
import net.rafkos.mc.plugins.KillTickets2.spigot.actions.ClearDatabaseAction;
import net.rafkos.mc.plugins.KillTickets2.spigot.actions.ClearPlayerKillsAction;
import net.rafkos.mc.plugins.KillTickets2.spigot.actions.ConfigReloadAction;
import net.rafkos.mc.plugins.KillTickets2.spigot.actions.ShowAllTicketsAction;
import net.rafkos.mc.plugins.KillTickets2.spigot.actions.ShowKilledPlayersAction;
import net.rafkos.mc.plugins.KillTickets2.spigot.actions.ShowPlayerTicketsAction;
import net.rafkos.mc.plugins.KillTickets2.spigot.actions.UseKillTicketAction;

public class RootCommandExecutor implements CommandExecutor
{

	@Override
	public boolean onCommand(CommandSender s, Command command, String label, String[] args)
	{
		if(!(s instanceof Player))
		{
			return false;
		}
		Player sender = (Player) s;
		if(args.length == 1)
		{
			if(args[0].equals("tickets"))
			{
				showAllTickets(sender);
				return true;
			}else if(args[0].equals("list"))
			{
				showKilledPlayers(sender);
				return true;
			}else if(args[0].equals("reload"))
			{
				reloadPluginConfig(sender);
				return true;
			}else if(args[0].equals("clear"))
			{
				clearKillsDatabase(sender);
				return true;
			}
		}else if(args.length == 2)
		{
			if(args[0].equals("list"))
			{
				UUID targetUniqueId = null;
				try
				{
					targetUniqueId = UUID.fromString(args[1]);
				}catch(Exception e)
				{
					sender.sendMessage(ChatColor.RED + "Podano nieprawidłowe UUID.");
				}
				showPlayerTickets(sender, targetUniqueId);
				return true;
			}else if(args[0].equals("clear"))
			{
				clearPlayerKills(sender, args);
				return true;
			}
		}else if(args.length == 3)
		{
			if(args[0].equals("use"))
			{
				useKillTicket(sender.getUniqueId(), args);
				return true;
			}
		}else if(args.length == 0)
		{
			showCommandsHelp(sender);
			return true;
		}

		return false;
	}

	private void reloadPluginConfig(Player sender)
	{
		try
		{
			SpigotApi.getActionTransmitter().send(new TargetBungeeServer(), ConfigReloadAction.class, new Object[]
			{ sender.getUniqueId() }, 2000L);
		}catch(ConnectionFailureException e)
		{
			e.printStackTrace();
		}
	}

	private void useKillTicket(UUID senderUniqueId, String[] args)
	{
		try
		{
			SpigotApi.getActionTransmitter().send(new TargetBungeeServer(), UseKillTicketAction.class, new Object[]
			{ senderUniqueId, args }, 2000L);
		}catch(ConnectionFailureException e)
		{
			e.printStackTrace();
		}
	}

	private void showPlayerTickets(Player sender, UUID targetUniqueId)
	{
		try
		{
			SpigotApi.getActionTransmitter().send(new TargetBungeeServer(), ShowPlayerTicketsAction.class, new Object[]
			{ sender.getUniqueId(), targetUniqueId }, 2000L);
		}catch(ConnectionFailureException e)
		{
			e.printStackTrace();
		}
	}

	private void clearKillsDatabase(Player sender)
	{
		try
		{
			SpigotApi.getActionTransmitter().send(new TargetBungeeServer(), ClearDatabaseAction.class, new Object[]
			{ sender.getUniqueId() }, 2000L);
		}catch(ConnectionFailureException e)
		{
			e.printStackTrace();
		}
	}

	private void clearPlayerKills(Player sender, String[] args)
	{
		try
		{
			SpigotApi.getActionTransmitter().send(new TargetBungeeServer(), ClearPlayerKillsAction.class, new Object[]
			{ sender.getUniqueId(), args }, 2000L);
		}catch(ConnectionFailureException e)
		{
			e.printStackTrace();
		}
	}

	private void showCommandsHelp(Player sender)
	{
		// user
		if(sender.hasPermission("KillTickets.user"))
		{
			sender.sendMessage("Dostępne komendy KillTickets:\n" + ChatColor.WHITE + "/ktickets list " + ChatColor.GRAY
					+ "- wyświetla klikalną listę zabitych graczy.\n" + ChatColor.WHITE
					+ "/ktickets list <UUID_przeciwnika> " + ChatColor.GRAY
					+ "- wyświetla listę biletów do wykorzystania na danym graczu.\n" + ChatColor.WHITE
					+ "/ktickets tickets " + ChatColor.GRAY + "- wyświetla wszystkie istniejące bilety.\n"
					+ ChatColor.WHITE + "/ktickets use <ID_biletu> <UUID_przeciwnika> " + ChatColor.GRAY
					+ "- używa biletu o danym ID na graczu (korzystanie z tej komendy jest niezalecane).\n");
		}
		// admin
		if(sender.hasPermission("KillTickets.admin"))
		{
			sender.sendMessage(ChatColor.WHITE + "/ktickets reload " + ChatColor.GRAY
					+ "- przeładowuje konfigurację pluginu.\n" + ChatColor.WHITE + "/ktickets clear " + ChatColor.GRAY
					+ "- usuwa bazę danych zabójstw.\n" + ChatColor.WHITE + "/ktickets clear <UUID/nick> "
					+ ChatColor.GRAY + "- usuwa zabójstwa danego gracza z bazy.");
		}
	}

	private void showAllTickets(Player sender)
	{
		try
		{
			SpigotApi.getActionTransmitter().send(new TargetBungeeServer(), ShowAllTicketsAction.class, new Object[]
			{ sender.getUniqueId() }, 2000L);
		}catch(ConnectionFailureException e)
		{
			e.printStackTrace();
		}
	}

	private void showKilledPlayers(Player player)
	{
		try
		{
			SpigotApi.getActionTransmitter().send(new TargetBungeeServer(), ShowKilledPlayersAction.class, new Object[]
			{ player.getUniqueId() }, 2000L);
		}catch(ConnectionFailureException e)
		{
			e.printStackTrace();
		}

	}

}
