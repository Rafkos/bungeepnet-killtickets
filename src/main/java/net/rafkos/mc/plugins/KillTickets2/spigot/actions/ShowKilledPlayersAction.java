/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.KillTickets2.spigot.actions;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.KillTickets2.bungee.KillTicketsBungee;
import net.rafkos.mc.plugins.KillTickets2.bungee.KillsInfo;
import net.rafkos.mc.plugins.KillTickets2.bungee.MessagesGenerator;

@AllArgsConstructor
@Getter
public class ShowKilledPlayersAction implements IAction
{
	private static final long serialVersionUID = -3807592027157694949L;
	private UUID senderUniqueId;

	@Override
	public HashMap<String, Object> execute()
	{
		ProxiedPlayer executor = ProxyServer.getInstance().getPlayer(senderUniqueId);
		if(executor.hasPermission("KillTickets.user"))
		{
			LinkedList<KillsInfo> list = KillTicketsBungee.getKillsDatabase().getKilledPlayersKillsInfos(senderUniqueId);
			if(list.isEmpty())
			{
				MessagesGenerator.message(executor, "Nie zabiłeś jeszcze nikogo lub wszystkie bilety wygasły.",
						ChatColor.RED);
			}else
			{
				MessagesGenerator.message(executor, "Lista zabitych przez ciebie graczy", ChatColor.WHITE);
				MessagesGenerator.message(executor, "(kliknij na imię aby zobaczyć bilety)", ChatColor.GRAY);
				MessagesGenerator.tellRawKilledPlayersList(executor, list);
			}
		}
		else
		{
			MessagesGenerator.message(executor, "Nie masz uprawnień do wykonania tej komendy.", ChatColor.RED);
		}
		return new HashMap<String, Object>();
	}

}
