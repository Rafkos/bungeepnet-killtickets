package net.rafkos.mc.plugins.KillTickets2.bungee;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * 
 * This class contains various methods for sending messages to players/server.
 * 
 * @author thebadogre
 *
 */
public class MessagesGenerator
{

	public static void tellRawTicketsList(ProxiedPlayer executor, String name, UUID uuid, ArrayList<Ticket> tickets,
			int killsCount, String expire, boolean online)
	{
		TextComponent tc = new TextComponent(name);
		tc.addExtra("  ");
		tc.addExtra(killsCount + "  [");

		for(int i = 0; i < tickets.size(); i++)
		{
			tc.addExtra(getClickableTicket(tickets.get(i), uuid));
			if(i + 1 != tickets.size())
			{
				tc.addExtra(", ");
			}
		}
		tc.addExtra("]  ");
		tc.addExtra(expire);
		tc.addExtra("  ");
		if(online)
		{
			TextComponent onlinec = new TextComponent("online");
			onlinec.setColor(ChatColor.GREEN);
			tc.addExtra(onlinec);
		}else
		{
			TextComponent offlinec = new TextComponent("offline");
			offlinec.setColor(ChatColor.RED);
			tc.addExtra(offlinec);
		}

		tc.addExtra("\n");
		TextComponent back = new TextComponent("Powrót");
		back.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
				new ComponentBuilder("<<kliknij>>").color(ChatColor.GOLD).create()));
		back.setColor(ChatColor.GREEN);
		back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ktickets list "));
		tc.addExtra(back);

		executor.sendMessage(tc);
	}

	private static TextComponent getClickableTicket(Ticket t, UUID uuid)
	{
		TextComponent tc = new TextComponent(t.getName());
		tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
				new ComponentBuilder(t.getDescription() + "\n<<użyj>>").color(ChatColor.GOLD).create()));
		tc.setColor(ChatColor.GREEN);
		tc.setClickEvent(
				new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ktickets use " + t.getTicketId() + " " + uuid));
		return tc;
	}

	public static void message(ProxiedPlayer executor, String message, net.md_5.bungee.api.ChatColor color)
	{
		TextComponent tc = new TextComponent(message);
		tc.setColor(color);
		executor.sendMessage(tc);
	}

	public static void tellRawKilledPlayersList(ProxiedPlayer executor, LinkedList<KillsInfo> list)
	{
		TextComponent tc = new TextComponent();

		for(KillsInfo ki : list)
		{
			TextComponent nick = new TextComponent(ki.getLastName());
			nick.setColor(ChatColor.GREEN);
			nick.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ktickets list " + ki.getUniqueId()));
			tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
					new ComponentBuilder("<<kliknij>>").color(ChatColor.GOLD).create()));
			tc.addExtra(nick);
			tc.addExtra(", ");
		}

		executor.sendMessage(tc);

	}

	public static void tellRawNewTickets(ProxiedPlayer executor, int currentKills, ProxiedPlayer victim,
			ArrayList<Ticket> unlockedTickets)
	{
		TextComponent tc = new TextComponent("Za zabicie ");
		tc.addExtra(victim.getName());
		tc.addExtra(" otrzymujesz ");
		if(unlockedTickets.size() > 1)
		{
			tc.addExtra("bilety: ");
		}else
		{
			tc.addExtra("bilet: ");
		}
		for(int i = 0; i < unlockedTickets.size(); i++)
		{
			TextComponent ticket = new TextComponent(unlockedTickets.get(i).getName());
			ticket.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
					new ComponentBuilder(unlockedTickets.get(i).getDescription()).color(ChatColor.GOLD).create()));
			ticket.setColor(ChatColor.GREEN);
			tc.addExtra(ticket);
			if(i + 1 != unlockedTickets.size())
			{
				tc.addExtra(", ");
			}
		}
		tc.addExtra(". Wpisz ");
		TextComponent cmd = new TextComponent("/ktickets list");
		cmd.setColor(ChatColor.GREEN);
		cmd.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ktickets list "));
		cmd.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
				new ComponentBuilder("<<kliknij>>").color(ChatColor.GOLD).create()));
		tc.addExtra(cmd);
		tc.addExtra(".");
		executor.sendMessage(tc);
	}

	public static void tellRawAllTickets(ProxiedPlayer executor, Collection<Ticket> values)
	{
		TextComponent tc = new TextComponent();

		for(Ticket t : values)
		{
			TextComponent ticket = new TextComponent(t.getTicketId());
			ticket.addExtra("  ");
			ticket.addExtra(t.getRequiredKills() + "  ");
			ticket.setColor(ChatColor.GRAY);

			if(!Tickets.canUseTicket(executor, t, Integer.MAX_VALUE))
			{
				TextComponent perm = new TextComponent("[brak uprawnień] ");
				perm.setColor(ChatColor.RED);
				ticket.addExtra(perm);
			}

			ticket.addExtra(t.getDescription());
			ticket.addExtra("\n");
			tc.addExtra(ticket);
		}

		executor.sendMessage(tc);
	}

}
