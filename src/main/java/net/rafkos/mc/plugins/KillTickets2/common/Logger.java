package net.rafkos.mc.plugins.KillTickets2.common;

public class Logger
{
	private static final String pluginName = "KillTickets";

	public static void warning(String message)
	{
		log(message, LogType.WARNING);
	}

	public static void error(String message)
	{
		log(message, LogType.ERROR);
	}

	public static void log(String message)
	{
		log(message, LogType.INFO);
	}

	public static void log(String message, LogType type)
	{
		StringBuilder sb = new StringBuilder("[");
		sb.append(pluginName).append("][");
		switch(type)
		{
		case ERROR:
			sb.append("ERROR");
			break;
		case INFO:
			sb.append("INFO");
			break;
		case WARNING:
			sb.append("WARNING");
			break;
		}
		sb.append("] ").append(message);
		if(!message.endsWith("."))
		{
			sb.append(".");
		}
		System.out.println(sb.toString());
	}

}
