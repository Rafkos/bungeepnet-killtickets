package net.rafkos.mc.plugins.KillTickets2.bungee;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.rafkos.mc.plugins.KillTickets2.common.Logger;

public class Config
{
	/**
	 * Json parser used in the plugin
	 */
	public final static Gson GSON = new Gson();
	public static long killExpireTimeMilliseconds = 3000000;
	public static long killsDatabaseCleanerTaskPeriod = 600;
	public static long killsDatabaseAutosaveTaskPeriod = 300;
	public static String killsDatabaseName = "default";
	private final static File configJson = new File("plugins/KillTickets2/config.json");

	public static boolean load()
	{
		try
		{
			JsonHelper config = Config.GSON.fromJson(new FileReader(configJson), JsonHelper.class);
			killExpireTimeMilliseconds = config.getKillExpireTimeMilliseconds();
			killsDatabaseCleanerTaskPeriod = config.getKillsDatabaseCleanerTaskPeriod();
			killsDatabaseName = config.getKillsDatabaseName();
			killsDatabaseAutosaveTaskPeriod = config.getKillsDatabaseAutosaveTaskPeriod();
			return true;
		}catch(JsonSyntaxException e)
		{
			e.printStackTrace();
		}catch(JsonIOException e)
		{
			e.printStackTrace();
		}catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		Logger.error("An error has occured during configuration loading");
		return false;
	}

	@AllArgsConstructor
	@Getter
	private static class JsonHelper
	{
		private String killsDatabaseName;
		private long killExpireTimeMilliseconds, killsDatabaseCleanerTaskPeriod, killsDatabaseAutosaveTaskPeriod;
	}
}
