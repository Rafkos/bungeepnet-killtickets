package net.rafkos.mc.plugins.KillTickets2.bungee;

import net.rafkos.mc.plugins.KillTickets2.common.Logger;

public class KillsDatabaseAutosave implements Runnable
{

	private KillsDatabase killsDatabase;

	public KillsDatabaseAutosave(KillsDatabase killsDatabase)
	{
		this.killsDatabase = killsDatabase;
	}

	@Override
	public void run()
	{
		Logger.log("Autosaving kills database");
		killsDatabase.save();
	}

}
