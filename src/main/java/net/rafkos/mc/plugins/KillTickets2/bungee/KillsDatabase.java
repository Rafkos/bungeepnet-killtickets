package net.rafkos.mc.plugins.KillTickets2.bungee;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.UUID;

import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.rafkos.mc.plugins.KillTickets2.common.Logger;

public class KillsDatabase
{
	@Getter
	private File databaseFile;
	@Getter
	private HashMap<UUID, HashMap<UUID, KillsInfo>> database;

	public KillsDatabase(String name)
	{
		databaseFile = new File("plugins/KillTickets2/" + name + ".database");
	}

	@SuppressWarnings("unchecked")
	public boolean load()
	{
		database = new HashMap<UUID, HashMap<UUID, KillsInfo>>();
		try
		{
			try
			{
				ObjectInputStream in = new ObjectInputStream(new FileInputStream(databaseFile));
				HashMap<UUID, HashMap<UUID, KillsInfo>> loadedDatabase = (HashMap<UUID, HashMap<UUID, KillsInfo>>) in
						.readObject();
				if(loadedDatabase != null)
				{
					database = loadedDatabase;
				}
				in.close();

			}catch(FileNotFoundException e)
			{
				// first run ?
			}

		}catch(IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
			Logger.warning("An error has occured during loading of kills database. Creating empty one instead.");
		}

		return true;
	}

	public boolean save()
	{
		Logger.log("Saving kills database...");
		try
		{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(databaseFile));
			out.writeObject(database);
			out.close();
			Logger.log("Kills database saved correctly");
			return true;
		}catch(IOException e)
		{
			e.printStackTrace();
			Logger.error("An error has occured during saving kills database");
		}
		return false;
	}

	public KillsInfo getKilledPlayerKillsInfo(UUID assasinUniqueId, UUID victimUniqueId)
	{
		if(database.containsKey(assasinUniqueId))
		{
			if(database.get(assasinUniqueId).containsKey(victimUniqueId))
			{
				return database.get(assasinUniqueId).get(victimUniqueId);
			}
		}
		return null;
	}

	public LinkedList<KillsInfo> getKilledPlayersKillsInfos(UUID uuid)
	{
		LinkedList<KillsInfo> infos = new LinkedList<>();
		if(database.containsKey(uuid))
		{
			for(Entry<UUID, KillsInfo> entry : database.get(uuid).entrySet())
			{
				if(!entry.getValue().expired())
				{
					infos.add(entry.getValue());
				}
			}
		}
		return infos;
	}

	public LinkedList<UUID> getKilledPlayersUUIDs(UUID uuid)
	{
		LinkedList<UUID> uuids = new LinkedList<>();
		if(database.containsKey(uuid))
		{
			for(Entry<UUID, KillsInfo> entry : database.get(uuid).entrySet())
			{
				if(!entry.getValue().expired())
				{
					uuids.add(entry.getKey());
				}
			}
		}
		return uuids;
	}

	public long getKillsExpireTimeMilliseconds(UUID assasin, UUID victim)
	{
		if(database.containsKey(assasin))
		{
			if(database.get(assasin).containsKey(victim))
			{
				KillsInfo ki = database.get(assasin).get(victim);
				if(!ki.expired())
				{
					return ki.getExpiresTime() - System.currentTimeMillis();
				}
			}
		}
		return 0;
	}

	public int getKillsCount(UUID assasinUniqueId, UUID victimUniqueId)
	{
		if(database.containsKey(assasinUniqueId))
		{
			if(database.get(assasinUniqueId).containsKey(victimUniqueId))
			{
				KillsInfo ki = database.get(assasinUniqueId).get(victimUniqueId);
				if(!ki.expired())
				{
					return ki.getKillsCount();
				}
			}
		}
		return 0;
	}

	public void registerKill(UUID assasinUniqueId, UUID victimUniqueId)
	{
		// check if player has no registered kills
		if(!database.containsKey(assasinUniqueId))
		{
			database.put(assasinUniqueId, new HashMap<UUID, KillsInfo>());
		}

		// check if player hadn't killed the victim before
		if(!database.get(assasinUniqueId).containsKey(victimUniqueId))
		{
			KillsInfo ki = new KillsInfo();
			database.get(assasinUniqueId).put(victimUniqueId, ki);
		}

		KillsInfo ki = database.get(assasinUniqueId).get(victimUniqueId);
		// check if previous kills are expired
		if(ki.expired())
		{
			ki.clear();
		}

		ProxiedPlayer pp = ProxyServer.getInstance().getPlayer(victimUniqueId);

		ki.refreshExpireTime();
		ki.setLastName(pp.getName());
		ki.setUniqueId(victimUniqueId);
		ki.increment();
	}

	public void unregisterKills(UUID assasinUniqueId, UUID victimUniqueId)
	{
		if(database.containsKey(assasinUniqueId))
		{
			if(database.get(assasinUniqueId).containsKey(victimUniqueId))
			{
				database.get(assasinUniqueId).remove(victimUniqueId);
			}
		}
	}

	public void unregisterPlayer(UUID assasinUniqueId)
	{
		if(database.containsKey(assasinUniqueId))
		{
			database.remove(assasinUniqueId);
		}
	}

	public void clearDatabase()
	{
		database.clear();
	}

	public void cleanDatabase()
	{
		for(Iterator<Entry<UUID, HashMap<UUID, KillsInfo>>> it = database.entrySet().iterator(); it.hasNext();)
		{
			Entry<UUID, HashMap<UUID, KillsInfo>> entry = it.next();
			cleanUUIDtoKillsInfoMap(entry.getValue());
			if(entry.getValue().isEmpty())
			{
				it.remove();
			}
		}
	}

	private void cleanUUIDtoKillsInfoMap(HashMap<UUID, KillsInfo> hashMap)
	{
		for(Iterator<Entry<UUID, KillsInfo>> it = hashMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<UUID, KillsInfo> entry = it.next();
			if(entry.getValue().expired())
			{
				it.remove();
			}
		}
	}

}
