package net.rafkos.mc.plugins.KillTickets2.bungee;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.rafkos.mc.plugins.KillTickets2.common.Logger;

public class Tickets
{
	private static final File ticketsJson = new File("plugins/KillTickets2/tickets.json");

	@Getter
	private static HashMap<String, Ticket> ticketIdToTicketMap = new HashMap<>();

	public static boolean load()
	{
		ticketIdToTicketMap = new HashMap<String, Ticket>();
		try
		{
			Ticket[] tickets = Config.GSON.fromJson(new FileReader(ticketsJson), Ticket[].class);
			for(Ticket t : tickets)
			{
				ticketIdToTicketMap.put(t.getTicketId(), t);
			}

			return true;
		}catch(JsonSyntaxException e)
		{
			e.printStackTrace();
		}catch(JsonIOException e)
		{
			e.printStackTrace();
		}catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}

		Logger.error("An error has occured during tickets loading");
		return false;
	}

	public static Ticket getTicketById(String id)
	{
		Ticket ticket = null;
		for(Ticket t : ticketIdToTicketMap.values())
		{
			if(t.getTicketId().equals(id))
			{
				ticket = t;
				break;
			}
		}
		return ticket;
	}

	public static ArrayList<Ticket> getTicketsForKillsExactly(int killsCount, ProxiedPlayer player)
	{
		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		for(Ticket t : ticketIdToTicketMap.values())
		{
			if(killsCount == t.getRequiredKills())
			{
				if(canUseTicket(player, t, killsCount))
				{
					tickets.add(t);
				}
			}
		}
		return tickets;
	}

	public static boolean canUseTicket(ProxiedPlayer player, Ticket t, int killsCount)
	{
		if(t.getPermissions().length == 0)
		{
			return true;
		}else
		{
			for(String permission : t.getPermissions())
			{
				if(player.hasPermission(permission))
				{
					return true;
				}
			}
		}
		return false;
	}

	public static ArrayList<Ticket> getTicketsForKills(int killsCount, ProxiedPlayer player)
	{
		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		for(Ticket t : ticketIdToTicketMap.values())
		{
			if(killsCount >= t.getRequiredKills())
			{
				if(canUseTicket(player, t, killsCount))
				{
					tickets.add(t);
				}
			}
		}
		return tickets;
	}
}
