/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.KillTickets2.spigot.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.KillTickets2.bungee.KillTicketsBungee;
import net.rafkos.mc.plugins.KillTickets2.bungee.KillsInfo;
import net.rafkos.mc.plugins.KillTickets2.bungee.MessagesGenerator;
import net.rafkos.mc.plugins.KillTickets2.bungee.Ticket;
import net.rafkos.mc.plugins.KillTickets2.bungee.Tickets;
import net.rafkos.mc.plugins.KillTickets2.common.TextFormatter;

@AllArgsConstructor
@Getter
public class ShowPlayerTicketsAction implements IAction
{
	private static final long serialVersionUID = -6942223893420652060L;
	private UUID senderUniqueId, targetUniqueId;

	@Override
	public HashMap<String, Object> execute()
	{
		ProxiedPlayer executor = ProxyServer.getInstance().getPlayer(senderUniqueId);

		if(executor.hasPermission("KillTickets.user"))
		{
			LinkedList<UUID> list = KillTicketsBungee.getKillsDatabase().getKilledPlayersUUIDs(senderUniqueId);

			if(!list.contains(targetUniqueId))
			{
				MessagesGenerator.message(executor, "Nie masz żadnych biletów do wykorzystania.", ChatColor.RED);
			}else
			{
				MessagesGenerator.message(executor, "Gracz | Zabicia | Bilety | Wygasa za | Status gracza",
						ChatColor.WHITE);

				int killsCount = KillTicketsBungee.getKillsDatabase().getKillsCount(senderUniqueId, targetUniqueId);
				KillsInfo ki = KillTicketsBungee.getKillsDatabase().getKilledPlayerKillsInfo(senderUniqueId,
						targetUniqueId);

				String name = targetUniqueId.toString();
				if(ki != null)
				{
					name = ki.getLastName();
				}

				ArrayList<Ticket> tickets = Tickets.getTicketsForKills(killsCount, executor);
				String expire = TextFormatter.getDurationBreakdown(KillTicketsBungee.getKillsDatabase()
						.getKillsExpireTimeMilliseconds(senderUniqueId, targetUniqueId));
				boolean online = false;
				ProxiedPlayer pp = ProxyServer.getInstance().getPlayer(targetUniqueId);
				if(pp != null)
				{
					if(pp.isConnected())
					{
						online = true;
					}
				}
				MessagesGenerator.tellRawTicketsList(executor, name, targetUniqueId, tickets, killsCount, expire,
						online);
			}
		}else
		{
			MessagesGenerator.message(executor, "Nie masz uprawnień do wykonania tej komendy.", ChatColor.RED);
		}
		return new HashMap<String, Object>();
	}

}
