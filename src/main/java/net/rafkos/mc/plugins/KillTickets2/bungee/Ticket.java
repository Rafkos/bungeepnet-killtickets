package net.rafkos.mc.plugins.KillTickets2.bungee;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Ticket implements Serializable
{
	private static final long serialVersionUID = 1222041008974184121L;
	private String ticketId;
	private String name;
	private int requiredKills;
	private String[] permissions;
	private String description;
	private TicketCommand[] commands;
	private boolean online, alive;

	@Override
	public String toString()
	{
		return ticketId;
	}
}
