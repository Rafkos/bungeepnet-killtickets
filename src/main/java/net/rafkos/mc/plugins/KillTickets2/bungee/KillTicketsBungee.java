/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.KillTickets2.bungee;

import java.io.File;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.rafkos.mc.plugins.KillTickets2.common.Logger;
import net.rafkos.utils.ResourceUnpacker.ResourceUnpacker;

public class KillTicketsBungee extends Plugin
{
	@Getter
	private static KillsDatabase killsDatabase;

	@Override
	public void onEnable()
	{
		ResourceUnpacker.copyConfigFilesFromJarIfMissing(this.getClass(), new File(this.getDataFolder().getPath()),
				"KillTickets", "config.json");
		ResourceUnpacker.copyConfigFilesFromJarIfMissing(this.getClass(), new File(this.getDataFolder().getPath()),
				"KillTickets", "tickets.json");

		if(!Tickets.load())
		{
			loadingError();
			return;
		}

		if(Config.load())
		{

			// setup kills database
			killsDatabase = new KillsDatabase(Config.killsDatabaseName);
			killsDatabase.load();

			// setup autosave task
			this.getProxy().getScheduler().schedule(this, new KillsDatabaseAutosave(killsDatabase), 60,
					Config.killsDatabaseAutosaveTaskPeriod, TimeUnit.SECONDS);

			// setup cleaner task
			this.getProxy().getScheduler().schedule(this, new KillsDatabaseCleaner(killsDatabase), 120,
					Config.killsDatabaseCleanerTaskPeriod, TimeUnit.SECONDS);
		}else
		{
			loadingError();
		}

	}

	private void loadingError()
	{
		// TODO Auto-generated method stub
		Logger.error("Unable to load configuration.");
	}

	@Override
	public void onDisable()
	{
		killsDatabase.cleanDatabase();
		killsDatabase.save();
	}

}
