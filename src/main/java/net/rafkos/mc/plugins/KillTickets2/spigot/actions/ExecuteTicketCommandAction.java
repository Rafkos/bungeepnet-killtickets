/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.KillTickets2.spigot.actions;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.ConsoleCommandSender;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.KillTickets2.bungee.TicketCommand;

@AllArgsConstructor
@Getter
public class ExecuteTicketCommandAction implements IAction
{
	private static final long serialVersionUID = -4665245639763339013L;
	private UUID senderUniqueId, targetUniqueId;
	private String senderName, targetName;
	private TicketCommand[] commands;
	private boolean online, alive;

	@Override
	public HashMap<String, Object> execute()
	{
		boolean used = false;
		String message = "";
		HashMap<String, Object> results = new HashMap<>();

		OfflinePlayer target = Bukkit.getPlayer(targetUniqueId);
		if(!target.isOnline() && (online || alive))
		{
			message = ChatColor.RED + "Aby użyć tego biletu gracz musi być online.";
		}else
		{
			if(target.isOnline() && alive)
			{
				if(target.getPlayer().isDead())
				{
					message = ChatColor.RED + "Aby użyć tego biletu gracz musi żyć.";
				}else
				{
					for(TicketCommand cmd : commands)
					{
						String updated = new String(cmd.getCommand());
						updated = updated.replace("%TARGET%", target.getName()).replace("%OWNER%", senderName);
						ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
						Bukkit.dispatchCommand(console, updated);
					}
					message = ChatColor.GREEN + "Bilet został pomyślnie użyty. Licznik zabójstw został wyzerowany.";
					used = true;
				}
			}

		}

		results.put("used", used);
		results.put("message", message);
		return results;
	}

}
