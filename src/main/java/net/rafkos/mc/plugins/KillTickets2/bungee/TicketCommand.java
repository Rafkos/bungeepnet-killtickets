package net.rafkos.mc.plugins.KillTickets2.bungee;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class TicketCommand implements Serializable
{
	private static final long serialVersionUID = 1004329007448958787L;
	private String command;
}
