package net.rafkos.mc.plugins.KillTickets2.spigot;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetBungeeServer;
import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;
import net.rafkos.mc.plugins.KillTickets2.spigot.actions.RegisterKillAction;

public class PlayerKillsListener implements Listener
{

	@EventHandler
	public void onPlayerDeathEvent(EntityDeathEvent e)
	{
		if(e.getEntity().getKiller() != null && e.getEntity().getType() == EntityType.PLAYER)
		{
			Player assasin = e.getEntity().getKiller();
			Player victim = (Player) e.getEntity();

			// ignore suicide
			if(assasin != victim)
			{
				try
				{
					SpigotApi.getActionTransmitter().send(new TargetBungeeServer(), RegisterKillAction.class,
							new Object[]
							{ assasin.getUniqueId(), victim.getUniqueId() }, 2000L);
				}catch(ConnectionFailureException e1)
				{
					e1.printStackTrace();
				}
			}
		}
	}

}
