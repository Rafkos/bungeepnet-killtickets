package net.rafkos.mc.plugins.KillTickets2.bungee;

import java.io.Serializable;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

public class KillsInfo implements Serializable
{
	private static final long serialVersionUID = 2065135960790089488L;
	@Getter
	private int killsCount = 0;
	@Getter
	@Setter
	private String lastName;
	@Getter
	@Setter
	private UUID uniqueId;
	@Getter
	private long expiresTime;

	public KillsInfo()
	{
		refreshExpireTime();
	}

	public void increment()
	{
		killsCount++;
	}

	public void clear()
	{
		killsCount = 0;
	}

	public boolean expired()
	{
		if(System.currentTimeMillis() > expiresTime)
		{
			return true;
		}
		return false;
	}

	public void refreshExpireTime()
	{
		expiresTime = System.currentTimeMillis() + Config.killExpireTimeMilliseconds;
	}
}
