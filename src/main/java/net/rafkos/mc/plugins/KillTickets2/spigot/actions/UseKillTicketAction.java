/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.KillTickets2.spigot.actions;

import java.util.HashMap;
import java.util.UUID;

import lombok.AllArgsConstructor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeApi;
import net.rafkos.mc.plugins.BungeePNet.common.Callback;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetPlayerUniqueId;
import net.rafkos.mc.plugins.KillTickets2.bungee.KillTicketsBungee;
import net.rafkos.mc.plugins.KillTickets2.bungee.MessagesGenerator;
import net.rafkos.mc.plugins.KillTickets2.bungee.Ticket;
import net.rafkos.mc.plugins.KillTickets2.bungee.Tickets;
import net.rafkos.mc.plugins.KillTickets2.bungee.actions.ExecuteKillTicketAction;

@AllArgsConstructor
public class UseKillTicketAction implements IAction
{
	private static final long serialVersionUID = 8012190542782739065L;
	private UUID senderUniqueId;
	private String[] args;

	@Override
	public HashMap<String, Object> execute()
	{

		ProxiedPlayer sender = ProxyServer.getInstance().getPlayer(senderUniqueId);
		if(sender.hasPermission("KillTickets.user"))
		{
			String ticketId = args[1];
			Ticket ticket = Tickets.getTicketById(ticketId);
			UUID targetUniqueId = null;
			try
			{
				targetUniqueId = UUID.fromString(args[2]);
			}catch(Exception e)
			{
				MessagesGenerator.message(sender, "Podano nieprawidłowe UUID.", ChatColor.RED);
				targetUniqueId = null;
			}

			if(targetUniqueId != null)
			{
				int killsCount = KillTicketsBungee.getKillsDatabase().getKillsCount(senderUniqueId, targetUniqueId);
				if(KillTicketsBungee.getKillsDatabase().getKilledPlayersUUIDs(senderUniqueId).contains(targetUniqueId))
				{
					if(ticket != null)
					{
						if(Tickets.getTicketsForKills(killsCount, sender).contains(ticket))
						{
							try
							{
								final UUID finalTargetUniqueId = UUID.fromString(targetUniqueId.toString());
								BungeeApi.getActionTransmitter().send(new TargetPlayerUniqueId(targetUniqueId),
										ExecuteKillTicketAction.class, new Object[]
										{ sender.getName(), targetUniqueId, ticket }, 2000L, new Callback((result ->
										{
											if((boolean) result.get("status"))
											{
												KillTicketsBungee.getKillsDatabase().unregisterKills(senderUniqueId,
														finalTargetUniqueId);
												MessagesGenerator.message(sender,
														"Bilet został pomyślnie użyty. Licznik zabójstw został wyzerowany.",
														ChatColor.GREEN);
											}
											if(result.containsKey("msg"))
											{
												MessagesGenerator.message(sender, result.get("msg").toString(),
														ChatColor.RED);
											}
										}), (error) ->
										{
											MessagesGenerator.message(sender,
													"Nie możesz używać podanego biletu na niezalogowanym graczu.",
													ChatColor.RED);
										}));
							}catch(ConnectionFailureException e)
							{
								e.printStackTrace();
							}
						}else
						{
							MessagesGenerator.message(sender, "Nie możesz używać podanego biletu.", ChatColor.RED);
						}
					}else
					{
						MessagesGenerator.message(sender, "Podany bilet nie istnieje.", ChatColor.RED);
					}

				}else
				{
					MessagesGenerator.message(sender, "Nie zabiłeś gracza o takiej nazwie.", ChatColor.RED);
				}
			}
		}else
		{
			MessagesGenerator.message(sender, "Nie masz uprawnień do wykonania tej komendy.", ChatColor.RED);
		}
		return new HashMap<>();
	}

}
